import VueRouter from "vue-router";
import Home from "./pages/Home";
import Attraction from "./pages/Attraction";
import AttractionCheck from "./pages/AttractionCheck";
import Food from "./pages/Food";
import FoodCheck from "./pages/FoodCheck";
import Education from "./pages/Education";
import EducationCheck from "./pages/EducationCheck";
import Contact from "./pages/Contact";

const routes = [
  { path: "/", component: Home },
  { path: "/attraction", component: Attraction },
  { path: "/attraction-check", component: AttractionCheck },
  { path: "/food", component: Food },
  { path: "/food-check", component: FoodCheck },
  { path: "/education", component: Education },
  { path: "/education-check", component: EducationCheck },
  { path: "/contact", component: Contact }
];
const appRoutes = new VueRouter({
  mode: "history",
  routes
});

export default appRoutes;
