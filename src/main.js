import Vue from "vue";
import VueRouter from "vue-router";
import appRoutes from "./router";
import App from "./App.vue";

Vue.config.productionTip = false;

Vue.use(VueRouter);

new Vue({ router: appRoutes, render: h => h(App) }).$mount("#app");
